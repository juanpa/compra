#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''



def main():
    habitual = ("patatas", "leche", "pan")
    especifica = []
    contespec = 0
    seguir = True
    while seguir == True:
        v = (input())
        especifica = especifica + [v]
        contespec = contespec + 1
        if v == "":
            especifica.remove(v)
            contespec = contespec - 1
            seguir = False

    for i in range(0, len(especifica)):
        print("Elemento a comprar:", especifica[i])

    for elemento in habitual:
        if elemento in especifica:
            especifica.remove(elemento)
    print("Lista de la compra:")
    for i in range(0, len(habitual)):
        print(habitual[i])
    for i in range(0, len(especifica)):
        print(especifica[i])
    print("Elementos habituales:", len(habitual))
    print("Elementos específicos:", contespec)
    print("Elementos en lista:", (len(habitual) + len(especifica)))


if __name__ == '__main__':
    main()
